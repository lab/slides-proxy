from aiohttp import web
import socketio

sio = socketio.AsyncServer(async_mode='aiohttp')
app = web.Application()
sio.attach(app)

@sio.on('multiplex-statechanged')
async def on_change(sid, data):
    del data['secret']
    await sio.emit(data['socketId'], data)


if __name__ == "__main__":
    web.run_app(app)
