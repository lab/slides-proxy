from setuptools import setup

setup(
    name="slides",
    version="0.0.0",
    url="https://github.com/realorangeone/slides",
    license="MIT",
    author="Jake Howard",
    description="Slides",
    packages=["slides"],
    include_package_data=True,
    zip_safe=False,
    pathon_requires=">=3.7",
    install_requires=[
        "aiohttp",
        "python-socketio",
        "gunicorn"
    ],
)
